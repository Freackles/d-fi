Ce projet a été fait sur Construct 2, Le fichier projet étant défi.capx
Ce projet est un jeu qui reprend un peu les même principes que Slay the spire mais au lieu de combiner des carte pour tuer des ennemis, là on les combines pour former des phrases gramaticalement correcte.

Amelioration possible: 
- Avoir plus de catégories de cartes (exemples: ponctuations, mots interogatives,...)
- Permettre au joueur de former des phrases plus longues

Le build se trouve dans l'archive Export.zip tandis que les fichiers sources (projet, quelques assets...) sont dans l'archive project.zip
